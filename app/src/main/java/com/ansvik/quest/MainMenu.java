package com.ansvik.quest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class MainMenu extends AppCompatActivity {
    public static final String SETTINGS = "Settings";
    public static final String FIRSTOPEN = "fopen";
    public static final String FNAME = "fname";
    public static final String DIRPATH = Environment.getExternalStorageDirectory() + "/.quests";

    static SharedPreferences settings;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        PreferenceManager.setDefaultValues(this, R.xml.pref_settings, false);

        settings = getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        if (!settings.contains(FIRSTOPEN)) {
            settings.edit().putBoolean(FIRSTOPEN, true).apply();

            Quest first = new Quest("MainQuest");
            QItem n = new QItem();
            n.setImageName("");
            n.setText("Hello! This is first quest.");
            n.addButton(1, "Click here!");
            first.items.add(n);
            n = new QItem();
            n.setImageName("");
            n.setText("This is second QItem!");
            n.addButton(-1, "Exit");
            first.items.add(n);

            File mainQuest = new File(DIRPATH);

            try {
                if (!mainQuest.exists()) {
                    mainQuest.mkdir();
                }
                mainQuest = new File(DIRPATH + "/mainQuest.json");
                if (!mainQuest.exists()) {
                    mainQuest.createNewFile();
                }
                Writer js = new FileWriter(mainQuest);
                Gson gs = new GsonBuilder().setPrettyPrinting().create();
                gs.toJson(first, js);
                js.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (settings.contains(Game.CONTINUE_QUEST)) {
            (findViewById(R.id.cont)).setVisibility(Button.VISIBLE);
        } else (findViewById(R.id.cont)).setVisibility(Button.GONE);
    }

    public void onMyClick(View view) {
        switch (view.getId()) {
            case R.id.start: {
                registerForContextMenu(view);
                openContextMenu(view);
                break;
            }
            case R.id.cont: {
                Intent intent = new Intent(this, Game.class);
                intent.putExtra(FNAME, settings.getString(Game.CONTINUE_QUEST, ""));
                settings.edit().remove(Game.CONTINUE_QUEST).apply();
                startActivity(intent);
                break;
            }
            case R.id.settings: {
                Intent intent = new Intent(this, Settings.class);
                startActivity(intent);
                break;
            }
            case R.id.editor: {
                Intent intent = new Intent(this, Editor.class);
                startActivity(intent);
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        File n = new File(DIRPATH);
        for (String x : n.list()) {
            if (x.contains(".json")) {
                menu.add(Menu.NONE, Menu.NONE, Menu.NONE, x.subSequence(0, x.length() - 5));
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (settings.contains(Game.CONTINUE_QUEST))
            settings.edit().remove(Game.CONTINUE_QUEST).apply();
        if (settings.contains(Game.CONTINUE_ID)) settings.edit().remove(Game.CONTINUE_ID).apply();
        Intent intent = new Intent(MainMenu.this, Game.class);
        intent.putExtra(FNAME, item.getTitle());
        startActivity(intent);
        return super.onContextItemSelected(item);
    }
}

