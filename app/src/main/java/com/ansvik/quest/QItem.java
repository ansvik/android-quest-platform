package com.ansvik.quest;

import java.util.ArrayList;
import java.util.List;

public class QItem {
    QItem(){
        text = "";
        imageName = "";
    }
    private String text, imageName;
    private List<QButton> buttons = new ArrayList<QButton>();
    public void setText(String text){
        this.text = text;
    }
    public void addButton(int toId, String text){
        buttons.add(new QButton(toId, text));
    }
    public void setImageName(String imageName){
        this.imageName = this.imageName;
    }
    public List<QButton> getButtons(){
        return buttons;
    }
    public String getText(){
        return text;
    }
    public String getImageName(){
        return imageName;
    }
}
