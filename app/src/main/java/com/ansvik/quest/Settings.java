package com.ansvik.quest;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;


public class Settings extends Activity {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        getFragmentManager().beginTransaction()
                .replace(R.id.settingsParent, new SettingsFragment())
                .commit();
    }
}
