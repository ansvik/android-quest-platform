package com.ansvik.quest;

import java.util.ArrayList;
import java.util.List;

public class Quest {
    Quest(String name){
        this.name = name;
    }
    private String name;
    public List<QItem> items = new ArrayList<QItem>();
    public String getName(){
        return name;
    }
}
