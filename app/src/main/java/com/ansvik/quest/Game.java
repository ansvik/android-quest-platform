package com.ansvik.quest;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class Game extends AppCompatActivity {

    public static final String CONTINUE_ID = "cId";
    public static final String CONTINUE_QUEST = "cQuest";

    TextView text;
    ImageView image;
    Quest thisQuest;
    public String QUEST_NAME;
    public String QUEST_PATH;
    public int id;
    public List<Integer> curIds = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);

        QUEST_NAME = getIntent().getStringExtra(MainMenu.FNAME);
        QUEST_PATH = MainMenu.DIRPATH + "/" + QUEST_NAME;
        text = findViewById(R.id.textView);
        image = findViewById(R.id.image);

        File fileQuest = new File(QUEST_PATH + ".json");
        Reader readerQuest;
        thisQuest = new Quest("NotLoaded");
        try {
            readerQuest = new FileReader(fileQuest);
            Gson jsonQuest = new GsonBuilder().setPrettyPrinting().create();
            thisQuest = jsonQuest.fromJson(readerQuest, Quest.class);
            Log.i("msg2", jsonQuest.toJson(thisQuest));
            readerQuest.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (MainMenu.settings.contains(CONTINUE_ID)) {
            id = MainMenu.settings.getInt(CONTINUE_ID, id);
            MainMenu.settings.edit().remove(CONTINUE_ID).apply();
        }
        else id = 0;

        float tSize = Float.parseFloat(PreferenceManager.getDefaultSharedPreferences(this).getString("text_size", ""));
        text.setTextSize(tSize);
        text.setTextColor(Color.WHITE);
        setItem();
    }

    View.OnClickListener n = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            for (QButton a : thisQuest.items.get(id).getButtons()){
                if(a.text == ((Button)view).getText()){
                    id = a.toId;
                    if(id < 0) {
                        finish();
                        return;
                    }
                    for(int curId : curIds) {
                        ((LinearLayout) findViewById(R.id.gameLayout)).removeView(findViewById(curId));
                    }
                    curIds.clear();
                    setItem();
                    return;
                }
            }
        }
    };

    private void setItem(){
        text.setText(thisQuest.items.get(id).getText());
        String img = thisQuest.items.get(id).getImageName();
        if(!img.isEmpty()){
            image.setImageURI(Uri.parse(QUEST_PATH + "/" + img));
            image.setVisibility(View.VISIBLE);
        }
        else image.setVisibility(View.GONE);
        for(QButton a : thisQuest.items.get(id).getButtons()){
            Button nB = new Button(this);
            nB.setId(View.generateViewId());
            curIds.add(nB.getId());
            nB.setText(a.text);
            nB.setOnClickListener(n);
            ((LinearLayout)findViewById(R.id.gameLayout)).addView(nB);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(id >= 0){
            MainMenu.settings.edit().putInt(CONTINUE_ID, id).apply();
            MainMenu.settings.edit().putString(CONTINUE_QUEST, QUEST_NAME).apply();
            return;
        }
        id = 0;
    }
}
